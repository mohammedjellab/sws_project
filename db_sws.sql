create database db_sws
use db_sws
--drop database db_sws
create table operations(
	id int primary key identity(1, 1),
	operationNum varchar(20) not null,
	operationName varchar(60),
	constraint uc_opNum unique (operationNum)
)

create table components(
	id int primary key identity(1, 1),
	componentSheet varchar(20),
	protoPN varchar(30),
	swsPN varchar(30) not null,
	partsName varchar(max),
	[type] varchar(30),
	family varchar(30),
	[size] varchar(30),
	supplier varchar(30),
	supplierPN varchar(max),
	npa varchar(30),
	constraint uc_swsPN unique(swsPN)
)

create table err_wrs(
	id int primary key identity(1, 1),
	checkCode varchar(10),
	swsPN varchar(30),
	swsName varchar(max),
	partType varchar(20),
	classFlag1 varchar(10),
	classFlag2 varchar(10),
	classFlag3 varchar(10),
	classFlag4 varchar(10),
	classFlag5 varchar(10),
	classFlag6 varchar(10),
	tempMH varchar(5),
	operationNum varchar(20),
	custC varchar(20),
	prdNum varchar(50),
	prdLevel varchar(20),
	areaC varchar(20),
	winUserID varchar(50),
	dateError datetime,
	urlDirectory varchar(max)
	constraint fk_swsPN foreign key (swsPN) references components(swsPN),
	constraint fk_opNum foreign key (operationNum) references operations(operationNum)
)
