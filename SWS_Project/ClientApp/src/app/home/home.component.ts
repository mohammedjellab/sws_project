import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Errors } from '../core/models/errors';
import { ErrWrsService } from '../core/services/api/err-wrs.service';
import { SharedService } from '../core/services/shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  file: File;

  formGroup: FormGroup;

  class_flag;
  errors: Errors[] = [];
  postedErrors: Errors[] = [];
  files: File[] = [];


  swsPnFromDB = [];
  // errWrs;

  constructor(private sharedService: SharedService,
    private errWrsService: ErrWrsService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {

    /*this.sharedService.class_flag_workbook$.subscribe(res => {
      this.class_flag = res;
      this.errors = res;
      console.log('New Value: ', res);
    });*/

    /*this.sharedService.errors_opr_obs$.subscribe(res => {
      if (res !== null && res.length > 0) {
        console.log('Error at 0: ', res[0].operationNum)
      } else console.log('Errors: ', res)
    });*/

    /*this.sharedService.errors_opr_obs$.subscribe(res => { 
      console.log('Source: ', res);
      this.class_flag = res;
      console.log('MAJ: ', this.class_flag);
    });*/

    this.sharedService.errors$.subscribe((res: Errors[]) => this.errors = res);
    this.errWrsService.getAllErrWrs().subscribe((res: string[]) => this.swsPnFromDB = res);

    this.formGroup = this.formBuilder.group({
      file: ['', Validators.required]
    });
  }

  submit() {
    console.log('Clicked');
    console.log(this.formGroup.value.file);
    console.log(this.errors);
  }

  compare() {
    console.log('This Errors: ', this.errors);
    console.log('This Checked: ', this.sharedService.checked());
    if (this.sharedService.checked()) {

      console.log('Posted Errors: ', this.errors);
      let formData = new FormData();
      console.log(this.formGroup.value.file);
      const files: File[] = this.formGroup.value.file;
      if (files.length > 0) {
        for (let i = 0; i < files.length; i++) {
          formData.append('files', files[i]);
        }
        console.log('FormData: ', formData);
      }
      // formData.append('files', files[0]);
      const data = {
        errors: this.errors,
        files: files
      };
      let checkOpNum = true;
      for (let i = 0; i < this.errors.length; i++) {
        if (this.errors[i].operationNum === '' && this.errors[i].operationNum === null) {
          checkOpNum = false;
          break;
        }
      }

      if (checkOpNum) {
        alert('All Op Num is set');
      } else {
        alert('Num Op Is empty');
      }

      formData.append('errors', JSON.stringify(this.errors));

      // console.log('Data: ', data);
      this.errWrsService.postErrWrs(formData).subscribe(res => console.log('Post: ', res));
    } else {
      this.openSnackBar('Is not matched');
      this.sharedService.changeFiles(null);
    }
  }

  openSnackBar(message) {
    this.snackBar.open(message);
  }
}
