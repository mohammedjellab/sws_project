﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWS_Project.Models
{
    public class ErrFiles
    {
        public List<ErrWrs> Errors { get; set; }
        // public List<Object> OperationNumArray { get; set; }
        // public List<IFormFile> Files { get; set; }
        // public IFormFile[] Files { get; set; }
    }
}
