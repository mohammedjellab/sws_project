﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace SWS_Project.Models
{
    public partial class SwsDBContext : DbContext
    {
        public SwsDBContext()
        {
        }

        public SwsDBContext(DbContextOptions<SwsDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Components> Components { get; set; }
        public virtual DbSet<ErrWrs> ErrWrs { get; set; }
        public virtual DbSet<Operations> Operations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("data source=.;initial catalog=db_sws;trusted_connection=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Components>(entity =>
            {
                entity.ToTable("components");

                entity.HasIndex(e => e.SwsPn)
                    .HasName("uc_swsPN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ComponentSheet)
                    .HasColumnName("componentSheet")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Family)
                    .HasColumnName("family")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Npa)
                    .HasColumnName("npa")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PartsName)
                    .HasColumnName("partsName")
                    .IsUnicode(false);

                entity.Property(e => e.ProtoPn)
                    .HasColumnName("protoPN")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Supplier)
                    .HasColumnName("supplier")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierPn)
                    .HasColumnName("supplierPN")
                    .IsUnicode(false);

                entity.Property(e => e.SwsPn)
                    .IsRequired()
                    .HasColumnName("swsPN")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ErrWrs>(entity =>
            {
                entity.ToTable("err_wrs");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AreaC)
                    .HasColumnName("areaC")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CheckCode)
                    .HasColumnName("checkCode")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClassFlag1)
                    .HasColumnName("classFlag1")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClassFlag2)
                    .HasColumnName("classFlag2")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClassFlag3)
                    .HasColumnName("classFlag3")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClassFlag4)
                    .HasColumnName("classFlag4")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClassFlag5)
                    .HasColumnName("classFlag5")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClassFlag6)
                    .HasColumnName("classFlag6")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CustC)
                    .HasColumnName("custC")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DateError)
                    .HasColumnName("dateError")
                    .HasColumnType("datetime");

                entity.Property(e => e.OperationNum)
                    .HasColumnName("operationNum")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PartType)
                    .HasColumnName("partType")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PrdLevel)
                    .HasColumnName("prdLevel")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PrdNum)
                    .HasColumnName("prdNum")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SwsName)
                    .HasColumnName("swsName")
                    .IsUnicode(false);

                entity.Property(e => e.SwsPn)
                    .HasColumnName("swsPN")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TempMh)
                    .HasColumnName("tempMH")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UrlDirectory)
                    .HasColumnName("urlDirectory")
                    .IsUnicode(false);

                entity.Property(e => e.WinUserId)
                    .HasColumnName("winUserID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.OperationNumNavigation)
                    .WithMany(p => p.ErrWrs)
                    .HasPrincipalKey(p => p.OperationNum)
                    .HasForeignKey(d => d.OperationNum)
                    .HasConstraintName("fk_opNum");

                entity.HasOne(d => d.SwsPnNavigation)
                    .WithMany(p => p.ErrWrs)
                    .HasPrincipalKey(p => p.SwsPn)
                    .HasForeignKey(d => d.SwsPn)
                    .HasConstraintName("fk_swsPN");
            });

            modelBuilder.Entity<Operations>(entity =>
            {
                entity.ToTable("operations");

                entity.HasIndex(e => e.OperationNum)
                    .HasName("uc_opNum")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.OperationName)
                    .HasColumnName("operationName")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.OperationNum)
                    .IsRequired()
                    .HasColumnName("operationNum")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
