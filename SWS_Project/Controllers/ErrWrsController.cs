﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SWS_Project.Models;

namespace SWS_Project.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ErrWrsController : ControllerBase
    {
        private readonly SwsDBContext _context;
        public static IWebHostEnvironment _environment;

        public ErrWrsController(SwsDBContext context, IWebHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        // GET: api/ErrWrs 
        [HttpGet]
        public /*async Task<ActionResult<*/IEnumerable<string>/*>>*/ GetErrWrs()
        {
            //return await _context.ErrWrs.ToListAsync();
            return _context.ErrWrs.Select(x => x.SwsPn);
        }

        // GET: api/ErrWrs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ErrWrs>> GetErrWrs(int id)
        {
            var errWrs = await _context.ErrWrs.FindAsync(id);

            if (errWrs == null)
            {
                return NotFound();
            }

            return errWrs;
        }

        // PUT: api/ErrWrs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutErrWrs(int id, ErrWrs errWrs)
        {
            if (id != errWrs.Id)
            {
                return BadRequest();
            }

            _context.Entry(errWrs).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ErrWrsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ErrWrs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        /*[HttpPost]
        public async Task<ActionResult<ErrWrs>> PostErrWrs(ErrWrs errWrs)
        {
            _context.ErrWrs.Add(errWrs);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetErrWrs", new { id = errWrs.Id }, errWrs);
        }*/


        /*[HttpPost, DisableRequestSizeLimit]
        public async Task<ActionResult<ErrWrs>> PostErrWrs(ErrFiles errWrs, [FromForm] IFormFileCollection files)
        {
            Console.WriteLine(errWrs);
            // _context.ErrWrs.Add(errWrs);
            // await _context.SaveChangesAsync();

            return Ok(new { message = "Good" });
            //return CreatedAtAction("GetErrWrs", new { id = errWrs.Id }, errWrs);
        }*/


        [HttpPost, DisableRequestSizeLimit]
        public async Task<ActionResult<ErrWrs>> PostErrWrs([FromForm] List<IFormFile> files, [FromForm] string errors)
        {
            //Console.WriteLine(errWrs);
            // _context.ErrWrs.Add(errWrs);
            // await _context.SaveChangesAsync();

            var errwrs = JsonConvert.DeserializeObject<List<ErrWrs>>(errors);
            Console.WriteLine(errwrs);
            // _context.ErrWrs.AddRange(obj);

            var components = await _context.Components.Where(w => errwrs.Select(e => e.SwsPn).Contains(w.SwsPn)).ToListAsync();
            var operations = await _context.Operations.Where(w => errwrs.Select(e => e.OperationNum).Contains(w.OperationNum)).ToListAsync();
            var errwsDB = await _context.ErrWrs.Where(w => errwrs.Select(e => e.SwsPn).Contains(w.SwsPn)).ToListAsync();

            var intersectionsComp = errwrs.Where(w => !components.Select(c => c.SwsPn).Contains(w.SwsPn)).ToList();
            var intersectionsOper = errwrs.Where(w => !operations.Select(o => o.OperationNum).Contains(w.OperationNum)).ToList(); // components.Intersect(errwrs.Select(x => x.SwsPn));
            var intersectionsErr = errwrs.Where(w => !errwsDB.Select(e => e.SwsPn).Contains(w.SwsPn)).ToList();



            if(intersectionsComp.Count > 0)
            {
                List<Components> cmp = intersectionsComp.Select(x => new Components { SwsPn = x.SwsPn }).ToList();
                _context.Components.AddRange(cmp);
                await _context.SaveChangesAsync();
            }

            if (intersectionsOper.Count > 0)
            {
                List<Operations> ops = intersectionsOper.GroupBy(g => new { g.OperationNum }).Select(x => new Operations { OperationNum = x.First().OperationNum }).ToList();
                _context.Operations.AddRange(ops);
                await _context.SaveChangesAsync();
            }

            if(intersectionsErr.Count > 0)
            {
                // _context.ErrWrs.AddRange(errwrs);
                string dbPath = await SaveFileAsync(files);
                errwrs.ForEach(f => f.UrlDirectory = dbPath);
                _context.ErrWrs.AddRange(errwrs);
                await _context.SaveChangesAsync();
            }

            // System.Diagnostics.Debug.WriteLine("Intersections: " + intersectionsComp);



            return Ok(new { message = "Good" });
            //return CreatedAtAction("GetErrWrs", new { id = errWrs.Id }, errWrs);
        }


        // DELETE: api/ErrWrs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ErrWrs>> DeleteErrWrs(int id)
        {
            var errWrs = await _context.ErrWrs.FindAsync(id);
            if (errWrs == null)
            {
                return NotFound();
            }

            _context.ErrWrs.Remove(errWrs);
            await _context.SaveChangesAsync();

            return errWrs;
        }

        private bool ErrWrsExists(int id)
        {
            return _context.ErrWrs.Any(e => e.Id == id);
        }


        public async Task<string> SaveFileAsync(List<IFormFile> files)
        {
            string folderName = "Uploads\\files_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
            string webRootPath = _environment.WebRootPath;
            string pathToSave = Path.Combine(webRootPath, folderName);

            if (!Directory.Exists(pathToSave))
            {
                Directory.CreateDirectory(pathToSave);
            }

            if (files.Count > 0)
            {
                foreach (IFormFile file in files)
                {
                    if (file.Length > 0)
                    {
                        var fullPath = Path.Combine(pathToSave, file.FileName);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }


                    }
                }
            }

            return pathToSave;
        }
    }
}
